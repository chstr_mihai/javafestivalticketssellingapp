package festival.persistence;


import festival.model.Show;

import java.time.LocalDate;

public interface ShowRepositoryInterface extends Repository<Long, Show> {

    Iterable<Show> getShowsOnDate(LocalDate date);

}
