package festival.persistence;


import festival.model.Ticket;

public interface TicketRepositoryInterface extends Repository<Long, Ticket> {

}
