package festival.persistence;


import festival.model.User;

public interface UserRepositoryInterface extends Repository<Long, User> {

    public User login(String email, String password);

}
