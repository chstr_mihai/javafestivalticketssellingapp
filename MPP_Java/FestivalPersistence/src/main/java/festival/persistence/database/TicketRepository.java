package festival.persistence.database;

import festival.model.Show;
import festival.model.Ticket;
import festival.persistence.Configuration;
import festival.persistence.JdbcUtils;
import festival.persistence.TicketRepositoryInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class TicketRepository implements TicketRepositoryInterface {

    private JdbcUtils dbUtils;
    private static final Logger logger = LogManager.getLogger();

    public TicketRepository() {
        Configuration.loadProperties(".\\FestivalPersistence\\persistence.config");
        Configuration.logger.info("Initializing UserDbRepository with {} ", Configuration.properties);
        dbUtils=new JdbcUtils(Configuration.properties);
    }

    @Override
    public Ticket getOne(Long tid) {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();
        Ticket ticket = null;

        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM tickets t INNER JOIN shows s ON s.sid = t.sid WHERE tid = ?")) {
            statement.setLong(1, tid);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {

                Long sid = resultSet.getLong("sid");
                String artist = resultSet.getString("artist");
                LocalDateTime dateTime = resultSet.getTimestamp("date_time").toLocalDateTime();
                String place = resultSet.getString("place");
                int availableSeats = resultSet.getInt("available_seats");
                int soldSeats = resultSet.getInt("sold_seats");
                Show show = new Show(sid, artist, dateTime, place, availableSeats, soldSeats);

                String buyer = resultSet.getString("buyer");
                int seatsCount = resultSet.getInt("seats_count");
                Long showId = resultSet.getLong("sid");
                ticket = new Ticket(tid, buyer, seatsCount, show);

            }

        } catch (SQLException ex) {
            logger.error(ex);
            System.err.println("DB Error " + ex);
        }

        logger.traceExit();
        return ticket;

    }

    @Override
    public Iterable<Ticket> getAll() {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();
        List<Ticket> tickets = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM tickets t INNER JOIN shows s ON s.sid = t.sid")) {
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {

                Long sid = resultSet.getLong("sid");
                String artist = resultSet.getString("artist");
                LocalDateTime dateTime = resultSet.getTimestamp("date_time").toLocalDateTime();
                String place = resultSet.getString("place");
                int availableSeats = resultSet.getInt("available_seats");
                int soldSeats = resultSet.getInt("sold_seats");
                Show show = new Show(sid, artist, dateTime, place, availableSeats, soldSeats);

                Long tid = resultSet.getLong("tid");
                String buyer = resultSet.getString("buyer");
                int seatsCount = resultSet.getInt("seats_count");
                Long showId = resultSet.getLong("sid");
                Ticket ticket = new Ticket(tid, buyer, seatsCount, show);
                tickets.add(ticket);

            }

        } catch (SQLException ex) {
            logger.error(ex);
            System.err.println("DB Error " + ex);
        }

        logger.traceExit();
        return tickets;

    }

    @Override
    public Ticket save(Ticket ticket) {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();

        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO tickets (buyer, seats_count, sid) VALUES(?,?,?)")) {
            statement.setString(1, ticket.getBuyer());
            statement.setInt(2, ticket.getSeatsCount());
            statement.setLong(3, ticket.getShow().getId());
            statement.executeUpdate();

        } catch (SQLException ex) {
            logger.error(ex);
            System.err.println("DB Error " + ex);
        }

        logger.traceExit();
        return ticket;

    }

    @Override
    public Ticket delete(Long tid) {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();
        Ticket ticket = getOne(tid);

        if (ticket != null) {

            try (PreparedStatement statement = connection.prepareStatement("DELETE FROM tickets WHERE tid = ?")) {
                statement.setLong(1, tid);
                statement.executeUpdate();

            } catch (SQLException ex) {
                logger.error(ex);
                System.err.println("DB Error " + ex);
            }

        }

        logger.traceExit();
        return ticket;

    }

    @Override
    public Ticket update(Ticket ticket) {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();
        Ticket foundTicket = getOne(ticket.getId());

        if (foundTicket != null) {

            try (PreparedStatement statement = connection.prepareStatement(
                    "UPDATE tickets SET buyer = ?, seats_count = ?, sid = ? WHERE tid = ?")) {
                statement.setString(1, ticket.getBuyer());
                statement.setInt(2, ticket.getSeatsCount());
                statement.setLong(3, ticket.getShow().getId());
                statement.setLong(4, ticket.getId());
                statement.executeUpdate();

            } catch (SQLException ex) {
                logger.error(ex);
                System.err.println("DB Error " + ex);
            }

        }

        logger.traceExit();
        return ticket;

    }

}