package festival.server;

import festival.model.exceptions.ServerException;
import festival.model.validators.ShowValidator;
import festival.model.validators.TicketValidator;
import festival.model.validators.UserValidator;
import festival.persistence.ShowRepositoryInterface;
import festival.persistence.TicketRepositoryInterface;
import festival.persistence.UserRepositoryInterface;
import festival.persistence.database.ShowRepository;
import festival.persistence.database.TicketRepository;
import festival.persistence.database.UserRepository;
import festival.server.servers.RpcConcurrentServer;
import festival.server.servers.Server;
import festival.services.*;

public class RpcServerStarter {

    private static final int defaultPort = 55555;

    public static void main(String[] args) {
        Configuration.logger.traceEntry();

        Configuration.loadProperties("D:\\facultate\\An2\\MPP\\projectMPP\\mpp-proiect-repository-ChocoStrutul\\MPP_Java\\FestivalServer\\server.config");

        Integer serverPort = defaultPort;
        try {
            serverPort = Integer.parseInt(Configuration.properties.getProperty("server.port"));
        } catch (NumberFormatException exception) {
            Configuration.logger.error("wrong port number {}", exception.getMessage());
            Configuration.logger.warn("using default port {}", defaultPort);
        }
        Configuration.logger.info("starting server on port {}", serverPort);

        UserRepositoryInterface userRepository = new UserRepository();
        ShowRepositoryInterface showRepository = new ShowRepository();
        TicketRepositoryInterface ticketRepository = new TicketRepository();

        UserValidator userValidator = new UserValidator();
        ShowValidator showValidator = new ShowValidator();
        TicketValidator ticketValidator = new TicketValidator();

        UserService userService = new UserService(userRepository, userValidator);
        ShowService showService = new ShowService(showRepository, showValidator);
        TicketService ticketService = new TicketService(ticketRepository, ticketValidator, showRepository);

        Services services = new BigService(userService, showService, ticketService);
        festival.server.Configuration.logger.trace("created {} instance", services);

        Server server = new RpcConcurrentServer(serverPort, services);
        festival.server.Configuration.logger.trace("created {} instance", server);
        try {
            server.start();
        } catch (ServerException exception) {
            festival.server.Configuration.logger.error("could not start the server", exception);
        } finally {
            try {
                server.stop();
            } catch (ServerException exception) {
                festival.server.Configuration.logger.error("could not stop the server", exception);
            }
        }

        festival.server.Configuration.logger.traceExit();
    }
}
