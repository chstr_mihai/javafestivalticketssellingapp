package festival.server.servers;

import festival.networking.rpcProtocol.ClientRpcProxy;
import festival.server.Configuration;
import festival.services.Services;

import java.net.Socket;

public class RpcConcurrentServer extends ConcurrentServer {

    private final Services services;

    public RpcConcurrentServer(Integer port, Services services) {
        super(port);
        this.services = services;
    }

    @Override
    protected Thread createClientProxyThread(Socket connection) {
        Configuration.logger.traceEntry("entering with", connection);

        ClientRpcProxy clientProxy = new ClientRpcProxy(services, connection);
        Thread clientProxyThread = new Thread(clientProxy);

        Configuration.logger.traceExit(clientProxyThread);
        return clientProxyThread;
    }
}
