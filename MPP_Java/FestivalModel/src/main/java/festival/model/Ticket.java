package festival.model;

public class Ticket extends Entity<Long> {
    private String buyer;
    private int seatsCount;
    private Show show;

    public Ticket(Long id, String buyer, int seatsCount, Show show) {
        setId(id);
        this.buyer = buyer;
        this.seatsCount = seatsCount;
        this.show = show;
    }

    public Ticket(String buyer, int seatsCount, Show show) {
        this.buyer = buyer;
        this.seatsCount = seatsCount;
        this.show = show;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public int getSeatsCount() {
        return seatsCount;
    }

    public void setSeatsCount(int seatsCount) {
        this.seatsCount = seatsCount;
    }

    public Show getShow() {
        return show;
    }

    public void setShow(Show show) {
        this.show = show;
    }

    @Override
    public String toString() {
        return "festival.model.Ticket " + getId() + ", name: " + buyer  + ", seatsCount: " + seatsCount + "  -->  festival.model.Show: " + show;
    }

}
