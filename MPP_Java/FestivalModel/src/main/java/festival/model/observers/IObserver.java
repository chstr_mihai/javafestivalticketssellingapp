package festival.model.observers;

public interface IObserver {
    void ticketSold(Long showId, Integer seatsCount);
}

