package festival.model;

public class User extends Entity<Long> {
    private String email;
    private String password;

    public User(Long id, String email, String password) {
        setId(id);
        this.email = email;
        this.password = password;
    }

    public User(Long id, String email) {
        setId(id);
        this.email = email;
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return getId() + ", " + email;
    }
}
