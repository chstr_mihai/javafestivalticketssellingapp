package festival.model.validators;

import festival.model.exceptions.*;

public interface Validator<T> {
    void validate(T entity) throws ValidationException;
}
