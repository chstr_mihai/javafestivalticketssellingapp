package musicFestival.services;

import musicFestival.domain.Show;
import musicFestival.domain.Validator;
import musicFestival.repository.ShowRepositoryInterface;

import java.time.LocalDate;

public class ShowService {

    private ShowRepositoryInterface showRepository;
    private Validator<Show> showValidator;

    public ShowService(ShowRepositoryInterface showRepository, Validator<Show> showValidator) {
        this.showRepository = showRepository;
        this.showValidator = showValidator;
    }

    public Iterable<Show> getAll() {

        return showRepository.getAll();

    }

    public Iterable<Show> getShowsOnDate(LocalDate date) {

        return showRepository.getShowsOnDate(date);

    }

}
