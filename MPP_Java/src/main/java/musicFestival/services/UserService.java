package musicFestival.services;

import musicFestival.domain.User;
import musicFestival.domain.Validator;
import musicFestival.repository.UserRepositoryInterface;

public class UserService {

    private UserRepositoryInterface userRepository;
    private Validator<User> userValidator;

    public UserService(UserRepositoryInterface userRepository, Validator<User> userValidator) {
        this.userRepository = userRepository;
        this.userValidator = userValidator;
    }

    public User login(String email, String password) {

        return userRepository.login(email, password);

    }

}
