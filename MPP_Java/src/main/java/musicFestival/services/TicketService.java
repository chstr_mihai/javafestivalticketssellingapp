package musicFestival.services;

import musicFestival.domain.Show;
import musicFestival.domain.Ticket;
import musicFestival.domain.Validator;
import musicFestival.repository.ShowRepositoryInterface;
import musicFestival.repository.TicketRepositoryInterface;

public class TicketService {

    private TicketRepositoryInterface ticketRepository;
    private Validator<Ticket> ticketValidator;

    private ShowRepositoryInterface showRepository;

    public TicketService(TicketRepositoryInterface ticketRepository, Validator<Ticket> ticketValidator, ShowRepositoryInterface showRepository) {
        this.ticketRepository = ticketRepository;
        this.ticketValidator = ticketValidator;

        this.showRepository = showRepository;
    }

    private void updateShowsSeatsInfo(Show show, int dif) {

        show.setSoldSeats(show.getSoldSeats() + dif);
        show.setAvailableSeats(show.getAvailableSeats() - dif);

        showRepository.update(show);

    }

    public Ticket save(Ticket ticket) {

        ticketValidator.validate(ticket);
        Ticket savedTicket = ticketRepository.save(ticket);
        updateShowsSeatsInfo(ticket.getShow(), ticket.getSeatsCount());
        return savedTicket;

    }

}
