package musicFestival;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import musicFestival.controllers.LoginController;
import musicFestival.domain.*;
import musicFestival.repository.*;
import musicFestival.services.ShowService;
import musicFestival.services.TicketService;
import musicFestival.services.UserService;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Properties;

public class MainFX extends Application {

    private UserRepositoryInterface userRepository;
    private ShowRepositoryInterface showRepository;
    private TicketRepositoryInterface ticketRepository;

    private UserService userService;
    private ShowService showService;
    private TicketService ticketService;

    private Validator<User> userValidator;
    private Validator<Show> showValidator;
    private Validator<Ticket> ticketValidator;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Properties properties = new Properties();
        try {
            properties.load(new FileReader("bd.config"));
        } catch (IOException e) {
            System.out.println("Cannot find bd.config " + e);
        }

        userRepository = new UserRepository(properties);
        showRepository = new ShowRepository(properties);
        ticketRepository = new TicketRepository(properties);

        userValidator = new UserValidator();
        showValidator = new ShowValidator();
        ticketValidator = new TicketValidator();

        userService = new UserService(userRepository, userValidator);
        showService = new ShowService(showRepository, showValidator);
        ticketService = new TicketService(ticketRepository, ticketValidator, showRepository);

        initStage();

    }

    void initStage() throws IOException {

        Stage stage = new Stage();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/login.fxml"));
        AnchorPane root=loader.load();

        LoginController loginController = loader.getController();
        loginController.setEnvironment(stage, userService, showService, ticketService);

        stage.setScene(new Scene(root));
        //stage.initStyle(StageStyle.UNDECORATED);
        stage.setTitle("Login");
        stage.show();

    }

}
