package musicFestival.domain;
import musicFestival.exceptions.ValidationException;

public interface Validator<T> {
    void validate(T entity) throws ValidationException;
}
