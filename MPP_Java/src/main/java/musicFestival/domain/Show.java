package musicFestival.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Show extends Entity<Long>{
    private String artist;
    private LocalDateTime dateTime;
    private String place;
    private Integer availableSeats;
    private Integer soldSeats;

    private String dateString;
    private String timeString;

    public Show(Long id, String artist, LocalDateTime dateTime, String place, Integer availableSeats, Integer soldSeats) {
        setId(id);
        this.artist = artist;
        this.dateTime = dateTime;
        this.place = place;
        this.availableSeats = availableSeats;
        this.soldSeats = soldSeats;
    }

    public Show(String artist, LocalDateTime dateTime, String place, Integer availableSeats, Integer soldSeats) {
        this.artist = artist;
        this.dateTime = dateTime;
        this.place = place;
        this.availableSeats = availableSeats;
        this.soldSeats = soldSeats;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Integer getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(Integer availableSeats) {
        this.availableSeats = availableSeats;
    }

    public Integer getSoldSeats() {
        return soldSeats;
    }

    public void setSoldSeats(Integer soldSeats) {
        this.soldSeats = soldSeats;
    }

    public String getDateString() {
        return dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
    }

    public String getTimeString() {
        return dateTime.format(DateTimeFormatter.ofPattern("HH:mm"));
    }

    @Override
    public String toString() {
        return getId() + ", Artist:" + artist + ", on " + dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + " at " + place + ", available seats="
                + availableSeats + ", sold seats=" + soldSeats;
    }
}
