package musicFestival.domain;

import musicFestival.exceptions.ValidationException;

public class TicketValidator implements Validator<Ticket> {

    @Override
    public void validate(Ticket ticket) throws ValidationException {

        String exp = "";

        if(ticket.getBuyer().equals(""))
            exp += "Invalid buyer's name!\n";

        if(ticket.getSeatsCount() < 0)
            exp += "Invalid seats count!\n";

        if(ticket.getShow() == null)
            exp += "Invalid show!\n";

        if (exp.length() > 0)
            throw new ValidationException(exp);

    }

}
