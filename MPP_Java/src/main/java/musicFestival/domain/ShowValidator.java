package musicFestival.domain;

import musicFestival.exceptions.ValidationException;

import java.time.LocalDateTime;

public class ShowValidator implements Validator<Show>{

    @Override
    public void validate(Show show) throws ValidationException {

        String exp = "";

        if(show.getArtist().equals(""))
            exp += "Invalid place!\n";

        if(show.getDateTime().compareTo(LocalDateTime.now()) < 0)
            exp += "Invalid date!\n";

        if(show.getPlace().equals(""))
            exp += "Invalid place!\n";

        if(show.getAvailableSeats() < 0)
            exp += "Invalid available seats value!\n";

        if(show.getSoldSeats() < 0)
            exp += "Invalid sold seats value!\n";

        if(exp.length() > 0)
            throw new ValidationException(exp);

    }

}
