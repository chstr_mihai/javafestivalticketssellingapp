package musicFestival.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import musicFestival.domain.User;
import musicFestival.exceptions.RepoException;
import musicFestival.services.ShowService;
import musicFestival.services.TicketService;
import musicFestival.services.UserService;

import java.io.IOException;

public class LoginController {

    public PasswordField textFieldPassword;
    public TextField textFieldEmail;
    public Label labelLoginError;

    private UserService userService;
    private ShowService showService;
    private TicketService ticketService;

    private Stage stage;

    public void setEnvironment(Stage stage, UserService userService, ShowService showService, TicketService ticketService) {

        this.stage = stage;
        this.userService = userService;
        this.showService = showService;
        this.ticketService = ticketService;

    }


    public void handleLogin(ActionEvent actionEvent) throws IOException {

        String email = textFieldEmail.getText();
        String password = textFieldPassword.getText();
        try {
            User loggedInUser = userService.login(email, password);
        } catch (RepoException ex) {
            labelLoginError.setText(ex.getMessage());
            return;
        }

        initMainWindow();

    }

    void initMainWindow() throws IOException {

        Stage mainStage = new Stage();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/main.fxml"));
        AnchorPane root=loader.load();

        MainController mainController = loader.getController();
        mainController.setEnvironment(mainStage, userService, showService, ticketService);

        mainStage.setScene(new Scene(root));
        //stage.initStyle(StageStyle.UNDECORATED);
        mainStage.setTitle("Main");

        stage.close();

        mainStage.show();

    }

}
