package musicFestival.repository;

import musicFestival.domain.Show;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface ShowRepositoryInterface extends Repository<Long, Show> {

    Iterable<Show> getShowsOnDate(LocalDate date);

}
