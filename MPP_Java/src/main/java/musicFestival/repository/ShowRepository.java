package musicFestival.repository;

import musicFestival.domain.Show;
import musicFestival.domain.Ticket;
import musicFestival.utils.JdbcUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ShowRepository implements ShowRepositoryInterface{

    private JdbcUtils dbUtils;
    private static final Logger logger= LogManager.getLogger();

    public ShowRepository(Properties properties) {
        logger.info("Initializing ShowRepository with properties: {} ", properties);
        dbUtils=new JdbcUtils(properties);
    }

    @Override
    public Show getOne(Long sid) {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();
        Show show = null;

        try(PreparedStatement statement = connection.prepareStatement("SELECT * FROM shows WHERE sid = ?")) {
            statement.setLong(1, sid);
            ResultSet resultSet = statement.executeQuery();

            if(resultSet.next()) {

                String artist = resultSet.getString("artist");
                LocalDateTime dateTime = resultSet.getTimestamp("date_time").toLocalDateTime();
                String place = resultSet.getString("place");
                int availableSeats = resultSet.getInt("available_seats");
                int soldSeats = resultSet.getInt("sold_seats");
                show = new Show(sid, artist, dateTime, place, availableSeats, soldSeats);

            }

        } catch (SQLException ex) {
            logger.error(ex);
            System.err.println("DB Error " + ex);
        }

        logger.traceExit();
        return show;

    }

    private List<Show> createShowArray(ResultSet resultSet) {

        List<Show> shows = new ArrayList<>();
        try {
            while (resultSet.next()) {

                Long sid = resultSet.getLong("sid");
                String artist = resultSet.getString("artist");
                LocalDateTime dateTime = resultSet.getTimestamp("date_time").toLocalDateTime();
                String place = resultSet.getString("place");
                int availableSeats = resultSet.getInt("available_seats");
                int soldSeats = resultSet.getInt("sold_seats");
                Show show = new Show(sid, artist, dateTime, place, availableSeats, soldSeats);
                shows.add(show);

            }
        } catch (SQLException ex) {
            logger.error(ex);
            System.err.println("DB Error " + ex);
        }

        return shows;

    }

    @Override
    public Iterable<Show> getAll() {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();
        List<Show> shows = null;

        try(PreparedStatement statement = connection.prepareStatement("SELECT * FROM shows")) {
            ResultSet resultSet = statement.executeQuery();

            shows = createShowArray(resultSet);

        } catch (SQLException ex) {
            logger.error(ex);
            System.err.println("DB Error " + ex);
        }

        logger.traceExit();
        return shows;

    }

    @Override
    public Show save(Show show) {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();

        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO shows (artist, date_time, place, available_seats, sold_seats) VALUES(?,?,?,?,?)")) {
            statement.setString(1, show.getArtist());
            statement.setTimestamp(2, Timestamp.valueOf(show.getDateTime()));
            statement.setString(3, show.getPlace());
            statement.setInt(4, show.getAvailableSeats());
            statement.setInt(5, show.getSoldSeats());
            statement.executeUpdate();

        } catch (SQLException ex) {
            logger.error(ex);
            System.err.println("DB Error " + ex);
        }

        logger.traceExit();
        return show;

    }

    @Override
    public Show delete(Long sid) {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();
        Show show = getOne(sid);

        if(show != null) {

            try (PreparedStatement statement = connection.prepareStatement("DELETE FROM shows WHERE sid = ?")) {
                statement.setLong(1, sid);
                statement.executeUpdate();

            } catch (SQLException ex) {
                logger.error(ex);
                System.err.println("DB Error " + ex);
            }

        }

        logger.traceExit();
        return show;

    }

    @Override
    public Show update(Show show) {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();
        Show foundShow = getOne(show.getId());

        if (foundShow != null) {

            try (PreparedStatement statement = connection.prepareStatement(
                    "UPDATE shows SET artist = ?, date_time = ?, place = ?, available_seats = ?, sold_seats = ? WHERE sid = ?")) {
                statement.setString(1, show.getArtist());
                statement.setTimestamp(2, Timestamp.valueOf(show.getDateTime()));
                statement.setString(3, show.getPlace());
                statement.setInt(4, show.getAvailableSeats());
                statement.setInt(5, show.getSoldSeats());
                statement.setLong(6, show.getId());
                statement.executeUpdate();

            } catch (SQLException ex) {
                logger.error(ex);
                System.err.println("DB Error " + ex);
            }

        }

        logger.traceExit();
        return show;

    }


    @Override
    public Iterable<Show> getShowsOnDate(LocalDate date) {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();
        List<Show> shows = new ArrayList<>();

        try(PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM shows WHERE DATE(date_time/1000,'unixepoch') = ?")) {
            statement.setString(1, date.toString());
            ResultSet resultSet = statement.executeQuery();

            shows = createShowArray(resultSet);

        } catch (SQLException ex) {
            logger.error(ex);
            System.err.println("DB Error " + ex);
        }

        logger.traceExit();
        return shows;

    }

}
