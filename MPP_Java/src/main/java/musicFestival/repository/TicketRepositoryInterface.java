package musicFestival.repository;

import musicFestival.domain.Ticket;

public interface TicketRepositoryInterface extends Repository<Long, Ticket> {

}
