package musicFestival.repository;

import musicFestival.domain.User;
import musicFestival.exceptions.RepoException;
import musicFestival.utils.JdbcUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class UserRepository implements UserRepositoryInterface{

    private JdbcUtils dbUtils;
    private static final Logger logger= LogManager.getLogger();

    public UserRepository(Properties properties) {
        logger.info("Initializing UserRepository with properties: {} ", properties);
        dbUtils=new JdbcUtils(properties);
    }

    @Override
    public User getOne(Long uid) {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();
        User user = null;

        try(PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE uid = ?")) {
            statement.setLong(1, uid);
            ResultSet resultSet = statement.executeQuery();

            if(resultSet.next()) {

                String email = resultSet.getString("email");
                user = new User(uid, email);

            }

        } catch (SQLException ex) {
            logger.error(ex);
            System.err.println("DB Error " + ex);
        }

        logger.traceExit();
        return user;

    }

    @Override
    public Iterable<User> getAll() {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();
        List<User> users = new ArrayList<>();

        try(PreparedStatement statement = connection.prepareStatement("SELECT * FROM users")) {
            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {

                Long uid = resultSet.getLong("uid");
                String email = resultSet.getString("email");
                User user = new User(uid, email);
                users.add(user);

            }

        } catch (SQLException ex) {
            logger.error(ex);
            System.err.println("DB Error " + ex);
        }

        logger.traceExit();
        return users;

    }

    private boolean verifyEmail(String email, Connection connection) {

        logger.traceEntry();

        boolean response = false;

        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE email = ?")) {
            statement.setString(1, email);
            ResultSet resultSet = statement.executeQuery();

            if(resultSet.next())
                response = true;

        } catch (SQLException ex) {
            logger.error(ex);
            System.err.println("DB Error " + ex);
        }

        logger.traceExit();
        return response;

    }

    @Override
    public User save(User user) {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();
        if(!verifyEmail(user.getEmail(), connection)) {

            try (PreparedStatement statement = connection.prepareStatement("INSERT INTO users (email, password) VALUES(?,?)")) {
                statement.setString(1, user.getEmail());
                statement.setString(2, user.getPassword());
                statement.executeUpdate();

            } catch (SQLException ex) {
                logger.error(ex);
                System.err.println("DB Error " + ex);
            }

        }
        else {

            try {
                connection.close();
            } catch (SQLException ex) {
                logger.error(ex);
                System.err.println("DB Error " + ex);
            }

        }

        logger.traceExit();
        return user;

    }

    @Override
    public User delete(Long uid) {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();
        User user = getOne(uid);

        if(user != null) {

            try (PreparedStatement statement = connection.prepareStatement("DELETE FROM users WHERE uid = ?")) {
                statement.setLong(1, uid);
                statement.executeUpdate();

            } catch (SQLException ex) {
                logger.error(ex);
                System.err.println("DB Error " + ex);
            }

        }

        logger.traceExit();
        return user;

    }

    @Override
    public User update(User user) {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();

        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE users SET email = ?, password = ? WHERE uid = ?")) {
            statement.setString(1, user.getEmail());
            statement.setString(2, user.getPassword());
            statement.setLong(3, user.getId());

            if(statement.executeUpdate() == 0)
                user = null;

        } catch (SQLException ex) {
            logger.error(ex);
            System.err.println("DB Error " + ex);
        }

        logger.traceExit();
        return user;

    }

    @Override
    public User login(String email, String password) {

        logger.traceEntry();

        Connection connection = dbUtils.getConnection();

        User user = null;
        try(PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM users WHERE email = ? and password = ?")) {
            statement.setString(1, email);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();

            if(resultSet.next()) {

                Long uid = resultSet.getLong("uid");
                user = new User(uid, email, password);

            } else {

                throw new RepoException("Invalid email or password!");

            }

        } catch (SQLException ex) {
            logger.error(ex);
            System.err.println("DB Error " + ex);
        }

        logger.traceExit();
        return user;

    }

}
