package musicFestival.repository;

import musicFestival.domain.User;

public interface UserRepositoryInterface extends Repository<Long, User> {

    public User login(String email, String password);

}
