package festival.client.controllers;

import festival.model.exceptions.*;
import festival.model.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


import java.io.IOException;

public class LoginController extends Controller {

    public PasswordField textFieldPassword;
    public TextField textFieldEmail;
    public Label labelLoginError;

    public void handleLogin(ActionEvent actionEvent) throws Exception {

        User loggedInUser = null;
        String email = textFieldEmail.getText();
        String password = textFieldPassword.getText();

        Stage mainStage = new Stage();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/main.fxml"));
        AnchorPane root=loader.load();

        MainController mainController = loader.getController();

        mainStage.setScene(new Scene(root));
        mainStage.setTitle("Main");

        try {
            loggedInUser = services.login(email, password, mainController);
            mainController.init(services, loggedInUser, mainStage);
        } catch (RepoException ex) {
            labelLoginError.setText(ex.getMessage());
            return;
        }

        stage.close();

        stage.setOnCloseRequest(event -> {
            try {
                mainController.handleLogout(null);
                System.exit(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.exit(0);
        });

        mainStage.show();

    }

}
