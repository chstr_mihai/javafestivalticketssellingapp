package festival.client.controllers;

import festival.client.Configuration;
import festival.model.User;
import festival.model.exceptions.*;
import festival.model.Show;
import festival.model.Ticket;
import festival.model.observers.IObserver;
import festival.services.Services;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MainController extends Controller implements IObserver {

//FXML ----------------------------------

    public TableView<Show> tableShows;
    public TableColumn<Show, String> showTableArtistColumn;
    public TableColumn<Show, String> showTableLocationColumn;
    public TableColumn<Show, String> showTableDateTimeColumn;
    public TableColumn<Show, Integer> showTableAvailableSeatsColumn;
    public TableColumn<Show, Integer> showTableSoldSeatsColumn;

    public TableView<Show> tableFilteredShows;
    public TableColumn<Show, String> filteredShowTableArtistColumn;
    public TableColumn<Show, String> filteredShowTableLocationColumn;
    public TableColumn<Show, String> filteredShowTableTimeColumn;
    public TableColumn<Show, Integer> filteredShowTableAvailableSeatsColumn;

    public Label labelFilterShowsError;
    public Label labelSellTicketError;

    public DatePicker datePickerForShows;
    public TextField textFieldSeats;
    public TextField textFieldArtist;
    public TextField textFieldDateTime;
    public TextField textFieldBuyerName;
    public TextField textFieldLocation;

//--------------------------------------

    private Show clickedShow = null;

    ObservableList<Show> modelShows = FXCollections.observableArrayList();
    ObservableList<Show> modelShowsByDate = FXCollections.observableArrayList();

    @Override
    public void init(Services services, User signedInUser, Stage stage) {
        super.init(services, signedInUser, stage);
        try {
            setTableShows();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTableShowsRowColor(TableView<Show> table) {

        table.setRowFactory(tv -> new TableRow<Show>(){
            @Override
            protected void updateItem(Show item, boolean empty) {
                super.updateItem(item,empty);
                if (empty || item == null) {
                    setStyle("");
                } else if (item.getAvailableSeats() <= 0){
                    setStyle("-fx-background-color: #ff0000");
                } else {
                    setStyle("");
                }
            }
        });

    }

    @FXML
    public void initialize() {

        showTableArtistColumn.setCellValueFactory(new PropertyValueFactory<>("artist"));
        showTableLocationColumn.setCellValueFactory(new PropertyValueFactory<>("place"));
        showTableDateTimeColumn.setCellValueFactory(new PropertyValueFactory<>("dateString"));
        showTableAvailableSeatsColumn.setCellValueFactory(new PropertyValueFactory<>("availableSeats"));
        showTableSoldSeatsColumn.setCellValueFactory(new PropertyValueFactory<>("soldSeats"));
        tableShows.setItems(modelShows);

        setTableShowsRowColor(tableShows);

        filteredShowTableArtistColumn.setCellValueFactory((new PropertyValueFactory<>("artist")));
        filteredShowTableLocationColumn.setCellValueFactory((new PropertyValueFactory<>("place")));
        filteredShowTableTimeColumn.setCellValueFactory((new PropertyValueFactory<>("timeString")));
        filteredShowTableAvailableSeatsColumn.setCellValueFactory((new PropertyValueFactory<>("availableSeats")));

        setTableShowsRowColor(tableFilteredShows);

    }

    private void setTableShows() throws Exception {

        this.modelShows.setAll(StreamSupport.stream(services.getAllShows().spliterator(), false).collect(Collectors.toList()));

    }

    private void setFilteredTableShows() throws Exception {

        Show show = tableFilteredShows.getSelectionModel().getSelectedItem();
        if(show == null)
            return;

        LocalDate date = show.getDateTime().toLocalDate();
        if(date == null)
            return;

        this.modelShowsByDate.setAll(StreamSupport.stream(services.getShowsOnDate(date).spliterator(), false).collect(Collectors.toList()));

    }

    public void handleFilterByDate(ActionEvent actionEvent) throws Exception {

        LocalDate date = datePickerForShows.getValue();

        if(date == null) {

            labelFilterShowsError.setText("You need to pick a date!");
            return;

        }

        labelFilterShowsError.setText("");
        tableShows.setVisible(false);
        tableFilteredShows.setVisible(true);

        modelShowsByDate.setAll(StreamSupport.stream(services.getShowsOnDate(date).spliterator(), false).collect(Collectors.toList()));
        tableFilteredShows.setItems(modelShowsByDate);

    }

    public void handleShowAll(ActionEvent actionEvent) {

        tableFilteredShows.setVisible(false);
        tableShows.setVisible(true);

    }

    public void handleLogout(ActionEvent actionEvent) throws IOException {

        try {
            services.logout(loggedInUser.getEmail(), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        stage.close();

    }
    
    public void handleSelectionChangedShowsTable(MouseEvent mouseEvent) {

        Show show = tableShows.getSelectionModel().getSelectedItem();
        if(show == null)
            return;

        textFieldArtist.setText(show.getArtist());
        textFieldDateTime.setText(show.getDateString());
        textFieldLocation.setText(show.getPlace());
        clickedShow = show;

    }

    public void handleSelectionChangedFilteredShowsTable(MouseEvent mouseEvent) {

        Show show = tableFilteredShows.getSelectionModel().getSelectedItem();
        if(show == null)
            return;

        textFieldArtist.setText(show.getArtist());
        textFieldDateTime.setText(show.getDateString());
        textFieldLocation.setText(show.getPlace());
        clickedShow = show;

    }

    private Ticket verifySellTicket() {

        if(clickedShow == null) {

            labelSellTicketError.setText("Pick a show from the list!");
            return null;

        }

        String buyer = textFieldBuyerName.getText();
        String seatsCountString = textFieldSeats.getText();

        if(buyer.equals("") || seatsCountString.equals("")){

            labelSellTicketError.setText("Fill all ticket info!");
            return null;

        }

        int seatsCount = Integer.parseInt(seatsCountString);

        if(seatsCount > clickedShow.getAvailableSeats()) {

            labelSellTicketError.setText("Not enough available seats!");
            return null;

        }

        return new Ticket(buyer, seatsCount, clickedShow);

    }

    public void handleSellTicket(ActionEvent actionEvent) throws Exception {
        
        Ticket ticket = verifySellTicket();
        if(ticket == null)
            return;

        try {
            services.sellTicket(ticket);
        } catch (ValidationException ex) {
            labelSellTicketError.setText(ex.getMessage());
            return;
        }

        setTableShows();
        setFilteredTableShows();

        labelSellTicketError.setText("");

    }

    private void updateShows(ObservableList<Show> model, Long showId, Integer seatsCount) {
        List<Show> shows = new ArrayList<>(model);

        for (Show show : shows) {
            if (show.getId().equals(showId)) {
                show.setAvailableSeats(show.getAvailableSeats() - seatsCount);
                show.setSoldSeats(show.getSoldSeats() + seatsCount);
                break;
            }
        }

        model.setAll(shows);
    }

    @Override
    public void ticketSold(Long showId, Integer seatsCount) {
        Configuration.logger.traceEntry("entering with {} and {}", showId, seatsCount);

        updateShows(modelShows, showId, seatsCount);
        updateShows(modelShowsByDate, showId, seatsCount);

        Configuration.logger.traceExit();

    }
}
