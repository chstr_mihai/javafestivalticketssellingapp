package festival.client.controllers;

import festival.client.Configuration;
import festival.model.User;
import festival.services.Services;
import javafx.stage.Stage;

public abstract class Controller {
    protected Services services = null;
    protected User loggedInUser = null;
    protected Stage stage;

    public void init(Services services, User signedInUser, Stage stage) {
        Configuration.logger.traceEntry("entering init with {} and {}", services, signedInUser);

        this.services = services;
        this.loggedInUser = signedInUser;
        this.stage = stage;

        Configuration.logger.traceExit();

    }
}
