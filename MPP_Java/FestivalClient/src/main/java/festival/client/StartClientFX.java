package festival.client;

import festival.client.controllers.LoginController;
import festival.networking.rpcProtocol.ServicesRpcProxy;
import festival.services.Services;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class StartClientFX extends Application {

    private static final int defaultPort = 55555;
    private static final String defaultServer = "localhost";

    @Override
    public void start(Stage primaryStage) throws Exception {

        Configuration.logger.traceEntry();
        Configuration.loadProperties("./FestivalClient/client.config");

        String serverIp = Configuration.properties.getProperty("server.host", defaultServer);
        Integer serverPort = defaultPort;

        try {
            serverPort = Integer.parseInt(Configuration.properties.getProperty("server.port"));
        } catch (NumberFormatException exception) {
            Configuration.logger.error("wrong port number {}", exception.getMessage());
            Configuration.logger.warn("using default port {}", defaultPort);
        }
        Configuration.logger.info("using server ip " + serverIp);
        Configuration.logger.info("using server port " + serverPort);

        Services services = new ServicesRpcProxy(serverIp, serverPort);
        Configuration.logger.trace("created {} instance", services);

        initStage(services);

        Configuration.logger.traceExit();

    }

    void initStage(Services services) throws IOException {

        Stage stage = new Stage();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/login.fxml"));
        AnchorPane root=loader.load();

        LoginController loginController = loader.getController();
        loginController.init(services, null, stage);

        stage.setScene(new Scene(root));
        stage.setTitle("Login");
        stage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}
