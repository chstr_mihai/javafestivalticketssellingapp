package festival.networking.rpcProtocol;

public enum ResponseType {
    OK,
    ERROR,
    SEATS_SOLD;
}
