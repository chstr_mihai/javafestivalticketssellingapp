package festival.networking.rpcProtocol;

import festival.model.Show;
import festival.model.Ticket;
import festival.model.User;
import festival.model.observers.IObserver;
import festival.networking.Configuration;
import festival.networking.dataTransfer.*;
import festival.services.Services;
import festival.model.exceptions.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.time.LocalDate;
import java.util.Collection;
import java.util.concurrent.LinkedBlockingQueue;

public class ServicesRpcProxy implements Services{

    private final String host;
    private final Integer port;
    private final LinkedBlockingQueue<Response> responses;
    private ObjectInputStream input = null;
    private ObjectOutputStream output = null;
    private Socket connection = null;
    private volatile boolean finished;
    private IObserver client;

    public ServicesRpcProxy(String host, Integer port) {
        this.host = host;
        this.port = port;
        responses = new LinkedBlockingQueue<>();
    }

    private void ensureConnection() throws NetworkingException {
        if (connection != null && input != null && output != null)
            return;

        try {
            connection = new Socket(host, port);
            output = new ObjectOutputStream(connection.getOutputStream());
            output.flush();
            input = new ObjectInputStream(connection.getInputStream());
            finished = false;
            Thread thread = new Thread(new ReaderThread());
            thread.start();
            Configuration.logger.trace("ensured connection {} {} {}", connection, input, output);
        } catch (IOException exception) {
            throw new NetworkingException("could not ensure connection");
        }
    }

    private void closeConnection() throws NetworkingException {
        finished = true;
        try {
            input.close();
            output.close();
            connection.close();
            client = null;
            Configuration.logger.trace("closed connection");
        } catch (IOException exception) {
            throw new NetworkingException("could not close connection");
        }

    }

    private void sendRequest(Request request) throws NetworkingException {
        ensureConnection();
        try {
            output.writeObject(request);
            output.flush();
            Configuration.logger.trace("sent request {}", request);
        } catch (IOException exception) {
            throw new NetworkingException("error sending object " + exception);
        }
    }

    private Response readResponse() throws NetworkingException {
        ensureConnection();
        Response response = null;
        try {
            response = responses.take();
            Configuration.logger.trace("taken response {} for reading", response);
        } catch (InterruptedException exception) {
            throw new NetworkingException("error reading object " + exception);
        }
        return response;
    }

    private void handleResponse(Response response) throws Exception {
        Configuration.logger.traceEntry("entering with {}", response);

        if (response.getType() == ResponseType.OK ||
                response.getType() == ResponseType.ERROR) {
            responses.put(response);

            Configuration.logger.traceExit();
            return;
        }

        String handlerName = "handle" + response.getType();
        try {
            Method method = this.getClass().getDeclaredMethod(handlerName, Response.class);
            method.invoke(this, response);

            Configuration.logger.traceExit();
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException exception) {
            throw new NetworkingException("unknown response");
        }
    }


    @Override
    public User login(String email, String password, IObserver client) throws Exception {

        Configuration.logger.traceEntry("entering with {} {} {}", email, password, client);

        User user = new User(email, password);
        UserDto userDto = DtoUtils.toDto(user);

        Request request = new Request.Builder().setType(RequestType.LOGIN).setData(userDto).build();
        sendRequest(request);

        Response response = readResponse();
        if (response.getType() == ResponseType.OK) {
            this.client = client;
            user = DtoUtils.toUser((UserDto) response.getData());

            Configuration.logger.traceExit(user);
            return user;
        } else if (response.getType() == ResponseType.ERROR) {
            throw new Exception(response.getData().toString());
        } else {
            throw new NetworkingException("received wrong response " + response.getType());
        }

    }

    @Override
    public void logout(String email, IObserver client) throws Exception {

        Configuration.logger.traceEntry("entering with {} {}", email, client);

        EmailDto emailDto = DtoUtils.toDto(email);

        Request request = new Request.Builder().setType(RequestType.LOGOUT).setData(emailDto).build();
        sendRequest(request);

        Response response = readResponse();
        closeConnection();
        if (response.getType() == ResponseType.ERROR) {
            throw new Exception(response.getData().toString());
        }

        Configuration.logger.traceExit();

    }

    @Override
    public Ticket sellTicket(Ticket ticket) throws Exception {

        Configuration.logger.traceEntry("entering with {}", ticket);

        TicketSellingDto ticketSellingDto = DtoUtils.toDto(ticket);

        Request request = new Request.Builder().setType(RequestType.SELL_TICKET).setData(ticketSellingDto).build();
        sendRequest(request);

        Response response = readResponse();
        if (response.getType() == ResponseType.ERROR) {
            throw new Exception(response.getData().toString());
        }

        Configuration.logger.traceExit();

        return null;
    }

    @Override
    public Iterable<Show> getAllShows() throws Exception {

        Configuration.logger.traceEntry();

        Request request = new Request.Builder().setType(RequestType.GET_ALL_SHOWS).build();
        sendRequest(request);

        Response response = readResponse();
        if (response.getType() == ResponseType.OK) {
            Collection<Show> shows = DtoUtils.toShowCollection((ShowCollectionDto) response.getData());

            Configuration.logger.traceExit(shows);
            return shows;
        } else if (response.getType() == ResponseType.ERROR) {
            throw new Exception(response.getData().toString());
        } else {
            throw new NetworkingException("received wrong response " + response.getType());
        }

    }

    @Override
    public Iterable<Show> getShowsOnDate(LocalDate date) throws Exception {

        Configuration.logger.traceEntry();

        DateDto dateDto = DtoUtils.toDto(date);
        Request request = new Request.Builder().setType(RequestType.GET_SHOWS_ON_DATE).setData(dateDto).build();
        sendRequest(request);

        Response response = readResponse();
        if (response.getType() == ResponseType.OK) {
            Collection<Show> shows = DtoUtils.toShowCollection((ShowCollectionDto) response.getData());

            Configuration.logger.traceExit(shows);
            return shows;
        } else if (response.getType() == ResponseType.ERROR) {
            throw new Exception(response.getData().toString());
        } else {
            throw new NetworkingException("received wrong response " + response.getType());
        }

    }

    private void handleSEATS_SOLD(Response response) {
        Configuration.logger.traceEntry("entering with {}", response);

        TicketSoldDto seatsSoldDto = (TicketSoldDto) response.getData();
        client.ticketSold(seatsSoldDto.getShowId(), seatsSoldDto.getSeatsCount());

        Configuration.logger.traceExit();
    }

    private class ReaderThread implements Runnable {

        public void run() {
            Configuration.logger.traceEntry();

            while (!finished) {
                try {
                    ensureConnection();
                    Configuration.logger.trace("waiting response");
                    Response response = (Response) input.readObject();
                    Configuration.logger.trace("response received {}", response);
                    handleResponse(response);
                    Configuration.logger.trace("response handled {}", response);
//                    Object response = input.readObject();
//                    if (isUpdate((Response) response)) {
//                        handleUpdate((Response) response);
//                    } else {
//                        responses.put((Response) response);
//                    }
                } catch (Exception exception) {
                    Configuration.logger.error(exception);
                }
            }

            Configuration.logger.traceExit();
        }
    }

}
