package festival.networking.dataTransfer;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ShowDto implements Serializable {
    private final Long id;
    private String artist;
    private LocalDateTime dateTime;
    private String place;
    private Integer availableSeats;
    private Integer soldSeats;

    public ShowDto(Long id, String artist, LocalDateTime dateTime, String place, Integer availableSeats, Integer soldSeats) {
        this.id = id;
        this.artist = artist;
        this.dateTime = dateTime;
        this.place = place;
        this.availableSeats = availableSeats;
        this.soldSeats = soldSeats;
    }

    public Long getId() {
        return id;
    }

    public String getArtist() {
        return artist;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getPlace() {
        return place;
    }

    public Integer getAvailableSeats() {
        return availableSeats;
    }

    public Integer getSoldSeats() {
        return soldSeats;
    }
}
