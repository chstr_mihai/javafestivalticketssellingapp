package festival.networking.dataTransfer;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class DateDto implements Serializable {
    private final LocalDate value;

    public DateDto(LocalDate value) {
        this.value = value;
    }

    public LocalDate getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "DateDto['" + value + ']';
    }
}
