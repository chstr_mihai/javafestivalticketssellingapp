package festival.networking.dataTransfer;

import java.io.Serializable;

public class TicketSellingDto implements Serializable {
    private final ShowDto showDto;
    private final String clientName;
    private final Integer seatsCount;

    public TicketSellingDto(ShowDto showDto, String clientName, Integer seatsCount) {
        this.showDto = showDto;
        this.clientName = clientName;
        this.seatsCount = seatsCount;
    }

    public ShowDto getShowDto() {
        return showDto;
    }

    public String getClientName() {
        return clientName;
    }

    public Integer getSeatsCount() {
        return seatsCount;
    }

    @Override
    public String toString() {
        return "SeatsCountDto[" + getShowDto() + " " + getClientName() + " " + getSeatsCount() + "]";
    }
}
