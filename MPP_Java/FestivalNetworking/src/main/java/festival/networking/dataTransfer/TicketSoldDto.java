package festival.networking.dataTransfer;

import java.io.Serializable;

public class TicketSoldDto implements Serializable {

    private final Long showId;
    private final Integer seatsCount;

    public TicketSoldDto(Long showId, Integer seatsCount) {
        this.showId = showId;
        this.seatsCount = seatsCount;
    }

    public Long getShowId() {
        return showId;
    }

    public Integer getSeatsCount() {
        return seatsCount;
    }

    public String toString() {
        return "SeatsSoldDto[" + getShowId() + " " + getSeatsCount() + "]";
    }
}
