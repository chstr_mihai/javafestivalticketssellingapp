package festival.networking.dataTransfer;

import java.io.Serializable;

public class ShowCollectionDto implements Serializable {
    private final ShowDto[] shows;

    public ShowCollectionDto(ShowDto[] shows) {
        this.shows = shows;
    }

    public ShowDto[] getShows() {
        return shows;
    }
}
