package festival.services;

import festival.model.Show;
import festival.model.Ticket;
import festival.model.User;
import festival.model.exceptions.NetworkingException;
import festival.model.observers.IObserver;

import java.time.LocalDate;

public interface Services {

    User login(String email, String password, IObserver client) throws Exception;

    void logout(String email, IObserver client) throws Exception;

    Ticket sellTicket(Ticket ticket) throws Exception;

    Iterable<Show> getAllShows() throws Exception;

    Iterable<Show> getShowsOnDate(LocalDate date) throws Exception;

}
