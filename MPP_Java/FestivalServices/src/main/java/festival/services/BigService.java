package festival.services;

import festival.model.Show;
import festival.model.Ticket;
import festival.model.User;
import festival.model.exceptions.LogInException;
import festival.model.exceptions.NetworkingException;
import festival.model.exceptions.ParameterException;
import festival.model.observers.IObserver;
import festival.persistence.Configuration;
import festival.persistence.ShowRepositoryInterface;
import festival.persistence.TicketRepositoryInterface;
import festival.persistence.UserRepositoryInterface;
import festival.persistence.database.ShowRepository;
import festival.persistence.database.TicketRepository;
import festival.persistence.database.UserRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BigService implements Services {

    private final Integer defaultThreadsCount = 5;
    private final UserService userService;
    private final ShowService showService;
    private final TicketService ticketService;
    private final Map<String, IObserver> signedInClients;

    public BigService(UserService userService, ShowService showService, TicketService ticketService) {
        this.userService = userService;
        this.showService = showService;
        this.ticketService = ticketService;
        signedInClients = new ConcurrentHashMap<>();
    }

    @Override
    public synchronized User login(String email, String password, IObserver client) throws Exception {

        if (signedInClients.containsKey(email))
            throw new LogInException("user already signed in");

        User loggedIn = userService.login(email, password);
        signedInClients.put(loggedIn.getEmail(), client);
        Configuration.logger.trace("user {} signed in", loggedIn);

        Configuration.logger.traceExit(loggedIn);

        return loggedIn;

    }

    @Override
    public synchronized void logout(String email, IObserver client) throws Exception {

        Configuration.logger.traceEntry("entering with {} {}", email, client);

        if (!signedInClients.containsKey(email))
            throw new LogInException("user already signed out");
        signedInClients.remove(email);
        Configuration.logger.trace("user {} signed out", email);

        Configuration.logger.traceExit();

    }

    @Override
    public synchronized Ticket sellTicket(Ticket ticket) {

        Ticket sold = ticketService.save(ticket);

        notifyTicketSold(ticket.getShow().getId(), ticket.getSeatsCount());

        return sold;

    }

    private void notifyTicketSold(Long showId, Integer seatsCount) {

        Configuration.logger.traceEntry();

        ExecutorService executor = Executors.newFixedThreadPool(defaultThreadsCount);
        for (IObserver client : signedInClients.values()) {
            executor.execute(() -> {
                client.ticketSold(showId, seatsCount);
            });
        }
        executor.shutdown();
        Configuration.logger.trace("notified clients of seats sold");

        Configuration.logger.traceExit();

    }

    @Override
    public synchronized Iterable<Show> getAllShows() {

        Configuration.logger.traceEntry();

        Iterable<Show> shows = showService.getAll();

        Configuration.logger.traceExit(shows);
        return shows;

    }

    @Override
    public synchronized Iterable<Show> getShowsOnDate(LocalDate date) {

        Configuration.logger.traceEntry();

        Iterable<Show> shows = showService.getShowsOnDate(date);

        Configuration.logger.traceExit(shows);
        return shows;

    }
}
