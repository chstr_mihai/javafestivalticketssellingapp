package festival.services;


import festival.model.Show;
import festival.model.Ticket;
import festival.model.validators.Validator;
import festival.persistence.ShowRepositoryInterface;
import festival.persistence.TicketRepositoryInterface;

public class TicketService {

    private TicketRepositoryInterface ticketRepository;
    private Validator<Ticket> ticketValidator;

    private ShowRepositoryInterface showRepository;

    public TicketService(TicketRepositoryInterface ticketRepository, Validator<Ticket> ticketValidator, ShowRepositoryInterface showRepository) {
        this.ticketRepository = ticketRepository;
        this.ticketValidator = ticketValidator;

        this.showRepository = showRepository;
    }

    private void updateShowsSeatsInfo(Show show, int dif) {

        show.setSoldSeats(show.getSoldSeats() + dif);
        show.setAvailableSeats(show.getAvailableSeats() - dif);

        showRepository.update(show);

    }

    public Ticket save(Ticket ticket) {

        ticketValidator.validate(ticket);
        Ticket savedTicket = ticketRepository.save(ticket);
        updateShowsSeatsInfo(ticket.getShow(), ticket.getSeatsCount());
        return savedTicket;

    }

}
